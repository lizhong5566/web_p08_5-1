﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("passAdmin")))
                {
                    displayMentorDropDownList();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }

            }          
        }

        //private void displayitemsetList() //display on grid view
        //{
        //    string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        //    SqlConnection conn = new SqlConnection(strConn);
        //    SqlCommand cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet", conn);
        //    SqlDataAdapter daSkillSet = new SqlDataAdapter(cmd);
        //    DataSet result = new DataSet();
        //    conn.Open();
        //    daSkillSet.Fill(result, "SkillSet");
        //    conn.Close();
        //    gv_skillset.DataSource = result;
        //    gv_skillset.DataBind();     
      
        //}

        private void displayMentorDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [Name],[MentorID] FROM Mentor", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Mentor");
            conn.Close();
            ddlMentor.DataSource = result.Tables["Mentor"];
            ddlMentor.DataTextField = "Name";
            ddlMentor.DataValueField = "MentorID";
            ddlMentor.DataBind();
            ddlMentor.Items.Insert(0, "--Select Mentor--");
        }

        protected void gv_skillset_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
               //Get the SelectedDataKey from GridView, which is the BranchNo,
               //and do the necessary data conversion.

               int selectedSkillSet = Convert.ToInt32(gv_skillset.SelectedDataKey[0]);

               //Create a skillset object.
               Skillset objSkillset = new Skillset();

               //Create a DataSet object to contain the staff list of a branch.
               DataSet result = new DataSet();

               objSkillset.SkillSetID = selectedSkillSet;
            
               int errorCode = objSkillset.getSkillSet(ref result);

               if (errorCode == 0)
               {
                   gv_skillset.DataSource = result;
                   gv_skillset.DataBind();
               }
               */
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "/Upload/" + FileUpload1.FileName.ToString());
            student objStudent = new student();
            objStudent.photo = FileUpload1.FileName;
            string name = txtName.Text;
            objStudent.name = txtName.Text;
            objStudent.mentorid = Convert.ToInt32(ddlMentor.SelectedValue);
            objStudent.course = ddlCourse.SelectedValue;
            string Email = txtEmailAddr.Text;
            objStudent.email = txtEmailAddr.Text;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("INSERT INTO STUDENT ([Name], EmailAddr, MentorID, Course,Photo ) " + "OUTPUT INSERTED.StudentID " + "VALUES(@name, @emailaddr, @mentorid, @course, @photo) ", conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text);
            cmd.Parameters.AddWithValue("@emailaddr", txtEmailAddr.Text);
            cmd.Parameters.AddWithValue("@mentorid", objStudent.mentorid);
            cmd.Parameters.AddWithValue("@course", objStudent.course);
            cmd.Parameters.AddWithValue("@photo", objStudent.photo);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
           
        }

        protected void cuvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                student Email = new student();

                if (Email.isEmailExist(txtEmailAddr.Text) == true)
                {
                    lblMessage.Text = "";
                    args.IsValid = false;
                }
                else
                {
                    lblMessage.Text = "Account Created!";
                    args.IsValid = true;
                }

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtEmailAddr.Text = string.Empty;
                txtName.Text = string.Empty;
            }
        }
    }
}

