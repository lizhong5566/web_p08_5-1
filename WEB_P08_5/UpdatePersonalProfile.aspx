﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="UpdatePersonalProfile.aspx.cs" Inherits="WEB_P08_5.UpdatePersonalProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">Current Email Address:</td>
            <td>
                &nbsp;
                <asp:Label ID="lbl_email" runat="server"></asp:Label>
&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">New Email Address:</td>
            <td>
                <asp:TextBox ID="txt_newemail" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_newemail" ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Skillset:</td>
            <td>
                <asp:DropDownList ID="ddlskillset" runat="server" OnSelectedIndexChanged="ddlskillset_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Achievements</td>
            <td>
                <asp:TextBox ID="txt_achievements" runat="server" OnTextChanged="txt_achievements_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Self-description:</td>
            <td>
                <asp:TextBox ID="txt_selfdescription" runat="server" Height="139px" Width="853px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" OnClick="btn_update_Click" Text="Update" />
            &nbsp;<br />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        </table>
</asp:Content>
