﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemAdminMenu.ascx.cs" Inherits="WEB_P08_5.SystemAdminMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <!-- the brand(or icon) of the navbar -->
    <a class="navbar-brand" href="SystemHome.aspx"
        style="font-size:32px; font-weight:bold; color:#3399FF;">
        System Admin
    </a>
    <!-- Toggle/Collapsible Button, also known as hamburger button -->
    <button class ="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Links in the navbar, displayed as drop-down list when collapsed -->
    <div class="collapse navbar-collapse" id ="staffNavbar">
        <!-- Links that are alighted to the left,
            mr-auto: right margin auto-adjusted -->
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="CreateMentor.aspx">Create Mentor</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" href="CreateStudent.aspx">Create Student</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Skillset.aspx">Create Skill Set</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" href="ViewParentAprovalRequest.aspx">Approve Viewing Request</a>
            </li>
             
            
        </ul>
        <!-- Links that are aligned to the right,
            ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A web form control button for logging out users -->
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out"
                    CssClass="btn btn-link nav-link" CausesValidation="false" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
    </nav>