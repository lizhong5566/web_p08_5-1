﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin.Master" AutoEventWireup="true" CodeBehind="Skillset.aspx.cs" Inherits="WEB_P08_5.Skillset1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 32px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">Skill Set Name</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtSkillSet" runat="server" Width="415px"></asp:TextBox>
                <asp:CustomValidator ID="cuvSkillSet" runat="server" Display="Dynamic" ErrorMessage="Skill Set Exist" ForeColor="Red" OnServerValidate="cuvSkillSet_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                <br />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
