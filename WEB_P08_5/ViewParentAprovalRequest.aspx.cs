﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ViewParentAprovalRequest : System.Web.UI.Page
    {
        string username = "Peter_Tan@yahoo.com";
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("passAdmin")))
                {
                    PopulateGridView();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }


            }
        void PopulateGridView()
        {
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("Select Parent.ParentName,ViewingRequest.StudentName,ViewingRequest.Status,ViewingRequest.ViewingRequestID,ViewingRequest.DateCreated from ViewingRequest Inner Join Parent On ViewingRequest.ParentID = Parent.ParentID Where ViewingRequest.Status ='R'", conn);
                oCmd.Fill(dtTbl);
            }
            gvReply.DataSource = dtTbl;
            gvReply.DataBind();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(btn.CommandArgument);
            string strConn = ConfigurationManager.ConnectionStrings
               ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("Update ViewingRequest set Status=@status where ViewingRequestID=" + id, conn);
            cmd.Parameters.AddWithValue("@status","A");
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            Response.Redirect("ViewParentAprovalRequest.aspx");
        }
    }
}