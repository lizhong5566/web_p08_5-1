﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class RequestPortfolioView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Parent")))
                {
                    displayStudentDropDownList();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void displayStudentDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [StudentID],[Name] FROM Student", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Student");
            conn.Close();
            ddlStudent.DataSource = result.Tables["Student"];
            ddlStudent.DataTextField = "Name";
            ddlStudent.DataValueField = "StudentID";
            ddlStudent.DataBind();
            ddlStudent.Items.Insert(0, "--Select Student--");
        }



        protected void btnCreate_Click(object sender, EventArgs e)
        {
            student stud = new student();

            int Pid = stud.getParentId(Session["LoginID"].ToString());
            int studID = Convert.ToInt32(ddlStudent.SelectedValue);
            string studentname = ddlStudent.SelectedItem.Text;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            //SqlCommand cmd = new SqlCommand
            //    ("SELECT StudentID FROM Student WHERE Name = @name", conn);
            //cmd.Parameters.AddWithValue("@name", studentname);

            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            //da.Fill(dt);

            //if (dt.Rows.Count > 0)
            //{
            student stud12 = new student();

               // int sid = stud12.getstudentId(studentname);
                char status = 'R';
                DateTime dat = DateTime.Now;
                SqlCommand cmd1 = new SqlCommand
                   ("INSERT INTO ViewingRequest VALUES(@pid,@name, @studentid, @Status, @date)", conn);
                cmd1.Parameters.AddWithValue("@pid", Pid);
                cmd1.Parameters.AddWithValue("@name", studentname);
                
                cmd1.Parameters.AddWithValue("@studentid", studID);
                cmd1.Parameters.AddWithValue("@Status", status);
                cmd1.Parameters.AddWithValue("@date", dat);
                cmd1.ExecuteScalar();
                conn.Close();
                lblMessage.Text = "Your Request of Viewing Portfolio Is Waiting for Admin Approval";
            //}
            //else
            //{
            //    lblErrorMessage.Text = "No Student With this name found";
            //}



        }
       
        

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}

