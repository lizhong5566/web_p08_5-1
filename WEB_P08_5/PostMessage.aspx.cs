﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class PostMessage : System.Web.UI.Page
    {
        int PID;
        int MID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Parent")))
            {
                ShowNames();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        public void ShowNames()
        {
            var strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                string oString = "Select Name, MentorID from Mentor";
                SqlCommand oCmd = new SqlCommand(oString, conn);
                conn.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        ddlMentorName.Items.Add(oReader["Name"].ToString());
                    }
                    conn.Close();
                }
            }
        }

        public void PostMsg()
        {

            var strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                string oString = "Select Name, MentorID from Mentor";
                SqlCommand oCmd = new SqlCommand(oString, conn);
                conn.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        MID = (int)oReader["MentorID"];
                    }
                    conn.Close();
                }
            }


            student stud = new student();

            string username = Session["LoginID"].ToString();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                SqlCommand oCmd = new SqlCommand("Select ParentID from Parent where EmailAddr = @user", conn);
                oCmd.Parameters.AddWithValue("@user", username);
                conn.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        PID = (int)oReader["ParentID"];
                    }
                    conn.Close();
                }
            }
            using (SqlConnection conn = new SqlConnection(strConn))
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Message(FromID, ToID, DateTimePosted, Title, Text)" +
                    "VALUES(@pid, @mid, @dtp, @title, @text) ";
                cmd.Parameters.AddWithValue("@pid", PID);
                cmd.Parameters.AddWithValue("@mid", MID);
                cmd.Parameters.AddWithValue("@dtp", DateTime.Now);
                cmd.Parameters.AddWithValue("@title", tbTitle.Text);
                cmd.Parameters.AddWithValue("@text", tbParentMsg.Text);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            PostMsg();
        }
    }
}