﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public class student
    {
        //porperties 
        public int studentID { get; set; }
        public string name { get; set; }
        public string course { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public string achievement { get; set; }
        public string externallink { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public char status { get; set; }
        public int mentorid { get; set; }
        public string skillset { get; set; }
        int sid;
        int pid;
        public int update() //update for student personal profile
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantiate a sqlconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);


            //Instantiate a sqlcommand object, supply it with a select sql
            //statement with retrieves all attributes of a student record.
            SqlCommand cmd = new SqlCommand
                ("UPDATE Student SET Achievement = @achievement, description = @description " + "WHERE StudentID = @StudentID", conn);

            //define the parameter used in sql statement, value for the 
            //parameter is retrieved from the branchNo property of the branch class.
            cmd.Parameters.AddWithValue("@achievement", achievement);
            //cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@StudentID", studentID);


            if (string.IsNullOrEmpty(description))
            {
                cmd.Parameters.AddWithValue("@description", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@description", description);
            }


            // A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //A connection should be closed after operations.
            conn.Close();

            if (count > 0) // at least 1 row update
                return 0; //update successful
            else
                return -2; // no update done
        }

        public int getDetails()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantiate a sqlconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);


            //Instantiate a sqlcommand object, supply it with a select sql
            //statement with retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("Select * FROM Student Where StudentID = @selectedStudentID", conn);


            //define the parameter used in sql statement, value for the 
            //parameter is retrieved from the branchNo property of the branch class.
            cmd.Parameters.AddWithValue("@selectedStudentID", studentID);

            //Instantiate a DataAdpater object, pass the sqlcommand
            //object created as paramater.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a dataset object result
            DataSet result = new DataSet();

            //open a database connection.
            conn.Open();

            //Use dataadpater to fetch data to a table "StaffDetails" in dataset
            daStudent.Fill(result, "StudentDetails");

            //Close database connection
            conn.Close();

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                DataTable table = result.Tables["StaffDetails"];
                //fill staff object with values from the dataset
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                { email = table.Rows[0]["Name"].ToString(); }

                else if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                { email = table.Rows[0]["EmailAddr"].ToString(); }

                else if (!DBNull.Value.Equals(table.Rows[0]["MentorID"]))
                { email = table.Rows[0]["MentorID"].ToString(); }



                return 0; // No error occurs
            }
            else
            {
                return -2; //record not found
            }
        }
        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings
                            ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("INSERT INTO STUDENT ([Name], EmailAddr ) " + "OUTPUT INSERTED.StudentID " + "VALUES(@name, @emailaddr) ", conn);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@email", email);
            conn.Open();
            int id = (int)cmd.ExecuteScalar();
            conn.Close();
            return id;
        }
        public bool isEmailExist(string email)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                       ("SELECT * FROM student WHERE EmailAddr=@selectedEmailAddr", conn);

            cmd.Parameters.AddWithValue("@selectedEmailAddr", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daEmail.Fill(result, "EmailDetails");
            conn.Close();

            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; // The Email exist           
            else
                return false; // The Email given does not exist

        }
        public int getstudentId(string usename)
        {
            
            string strConn = ConfigurationManager.ConnectionStrings
                             ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand
                ("SELECT StudentID FROM Student WHERE EmailAddr = @uname", conn);

            cmd.Parameters.AddWithValue("@uname", usename);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            // using (SqlDataReader dr = cmd.ExecuteReader())
            if(dr.Read())
            {
                sid =(int)dr["StudentID"];
            }
            return sid;
        }

        public int getParentId(string usename1)
        {

            string strConn = ConfigurationManager.ConnectionStrings
                             ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand
                ("SELECT ParentID FROM Parent WHERE EmailAddr = @uname1", conn);

            cmd.Parameters.AddWithValue("@uname1", usename1);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            // using (SqlDataReader dr = cmd.ExecuteReader())
            if (dr.Read())
            {
                pid = (int)dr["ParentID"];
            }
            return pid;
        }
    }

}
