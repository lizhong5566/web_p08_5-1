﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="MyPersonalProfile.aspx.cs" Inherits="WEB_P08_5.MyPersonalProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 142px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     
    <p>
        &nbsp;</p>
    <p>
        <asp:GridView ID="gv_profile" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="gv_profile_SelectedIndexChanged" AutoGenerateSelectButton="True" Width="415px">
            <Columns>
                <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
            </Columns>
        </asp:GridView>
        <table class="w-100">
            <tr>
                <td class="auto-style1">Name:</td>
                <td>
                    <asp:Label ID="lblname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">Achievement:</td>
                <td>
                    <asp:Label ID="lblachievement" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">About Me:</td>
                <td>
                    <asp:Label ID="lblaboutme" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>
                    <asp:Button ID="btn_Update" runat="server" OnClick="btn_Update_Click" Text="Update" />
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>
</asp:Content>
