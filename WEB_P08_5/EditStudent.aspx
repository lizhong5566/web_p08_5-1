﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor.Master" AutoEventWireup="true" CodeBehind="EditStudent.aspx.cs" Inherits="WEB_P08_5.Mentor.EditStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellpadding="0" cellspacing="0" class="w-100">
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td><strong>Update Approval Status</strong></td>
        </tr>
        <tr>
            <td class="auto-style1">Student Id:</td>
            <td>
                <asp:Label ID="lblid" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Name:</td>
            <td>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Course:</td>
            <td>
                <asp:Label ID="LabCourse" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Description:</td>
            <td>
                <asp:Label ID="LabDescription" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Photo:</td>
            <td>
                  <img width="100" height="100"  src=<%# string.Format("/Upload/{0}",LabPhoto)%> />
                <asp:Label ID="LabPhoto" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="auto-style1">Achievement:</td>
            <td>
                <asp:Label ID="LabAchievement" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="auto-style1">ExternalLink:</td>
            <td>
                <asp:Label ID="LabExternalLink" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="auto-style1">Projects:</td>
            <td>
                <asp:GridView ID="gvProject" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ProjectID" >
        <Columns>
           
        <asp:TemplateField HeaderText="ProjectID">
    <ItemTemplate>
        <asp:Label ID="LabelImage" runat="server" Text='<%# Eval("ProjectID") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox ID="txt_Image" runat="server" Text='<%# Eval("ProjectID") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>   
<asp:TemplateField HeaderText="Role">
    <ItemTemplate>
        <asp:Label ID="LabelImage" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox ID="txt_Image" runat="server" Text='<%# Eval("Role") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>
             
        </Columns>
    </asp:GridView>
            </td>
        </tr>
        <%--<tr>
            <td class="auto-style1">Status:</td>
            <td>
                <asp:DropDownList ID="ddlStatus" runat="server">
                </asp:DropDownList>
            </td>
        </tr>--%>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Approve" CausesValidation="false"  OnClick="btnSave_Click" />
                <br />
                <asp:Label ID="lblMessage" ForeColor="Red" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
     <table class="w-100">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
        </tr>
       
       <tr>
        <tr>
            <td class="auto-style2">Suggestion :</td>
            <td class="auto-style3">
               
                <asp:TextBox ID="txtEmailAddr" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="Please enter Suggestion" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
      
      <tr>
          <td>

          </td>
          <td>
                <asp:Label ID="Label1" runat="server" ForeColor="Red" ></asp:Label>
          </td>
      </tr>
        
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                 <asp:Label ID="Label2" runat="server" ForeColor="Red" ></asp:Label>
                <br />
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click1" />
         
            </td>
        </tr>
    </table>
</asp:Content>
