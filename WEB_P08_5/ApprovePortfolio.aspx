﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor.Master" AutoEventWireup="true" CodeBehind="ApprovePortfolio.aspx.cs" Inherits="WEB_P08_5.Mentor.ApprovePortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        <strong>Approve Portfolios :&nbsp;&nbsp;&nbsp; </strong>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:GridView ID="gvStudent" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="EditStudent.aspx?studentid={0}" DataTextField="StudentID" HeaderText="Student ID" />
                <asp:BoundField DataField="Name" HeaderText="Student Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </p>
</asp:Content>
