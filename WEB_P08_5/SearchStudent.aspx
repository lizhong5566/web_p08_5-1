﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor.Master" AutoEventWireup="true" CodeBehind="SearchStudent.aspx.cs" Inherits="WEB_P08_5.SearchStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <table class="w-100">
     <tr>
            <td class="auto-style2">Select Skill:</td>
            <td class="auto-style3">
               
               <asp:DropDownList ID="ddlSKill" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlSKill" Display="Dynamic" ErrorMessage="Please select Mentor" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
         <tr>
             <td>

             </td>
             <td>OR</td>
         </tr>
      <tr>
          <tr>
            <td class="auto-style2">Select Student:</td>
            <td class="auto-style3">
               
               <asp:DropDownList ID="ddlStudent" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlStudent" Display="Dynamic" ErrorMessage="Please select Mentor" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                    
                <asp:Button ID="btnCreate" runat="server" Text="Search" OnClick="btnCreate_Click" />
         <br />
                 <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <p>
        Search Results</p>
    <p>
        <asp:GridView ID="gvReply" runat="server">
        </asp:GridView>
    </p>
</asp:Content>
