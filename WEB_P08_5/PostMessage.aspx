﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Parent.Master" AutoEventWireup="true" CodeBehind="PostMessage.aspx.cs" Inherits="WEB_P08_5.PostMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 476px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style1">Post Message to Mentor<br />
                <br />
                Mentor:<br />
                <br />
                Title:<br />
                <br />
                Message:<br />
                <br />
                <br />
            </td>
            <td>
                <br />
                <br />
                <asp:DropDownList ID="ddlMentorName" runat="server" Width="245px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:TextBox ID="tbTitle" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:TextBox ID="tbParentMsg" runat="server" TextMode="MultiLine" Width="500px"></asp:TextBox>
                <br />
                <br />
                <asp:Button ID="btnSendMsg" runat="server" OnClick="btnSendMsg_Click" Text="Send" />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
