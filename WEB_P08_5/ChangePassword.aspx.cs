﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Mentor")))
            {
                
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        //private void displayitemsetList() //display on grid view
        //{
        //    string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        //    SqlConnection conn = new SqlConnection(strConn);
        //    SqlCommand cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet", conn);
        //    SqlDataAdapter daSkillSet = new SqlDataAdapter(cmd);
        //    DataSet result = new DataSet();
        //    conn.Open();
        //    daSkillSet.Fill(result, "SkillSet");
        //    conn.Close();
        //    gv_skillset.DataSource = result;
        //    gv_skillset.DataBind();

        //}

        //private void displayMentorDropDownList()
        //{
        //    string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        //    SqlConnection conn = new SqlConnection(strConn);
        //    SqlCommand cmd = new SqlCommand("SELECT [Name] FROM Mentor", conn);
        //    SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
        //    DataSet result = new DataSet();
        //    conn.Open();
        //    daMentor.Fill(result, "Mentor");
        //    conn.Close();
        //    ddlMentor.DataSource = result.Tables["Mentor"];
        //    ddlMentor.DataTextField = "Name";
        //    ddlMentor.DataValueField = "Name";
        //    ddlMentor.DataBind();
        //    ddlMentor.Items.Insert(0, "--Select--");
        //}

     

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtPass.Text == txtCnfPass.Text)
            {

                Mentors obj = new Mentors();

                int mentorid = obj.getmentorId(Session["LoginID"].ToString());
                string strConn = ConfigurationManager.ConnectionStrings
                    ["Student_EPortfolio"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand
                        (" UPDATE Mentor SET Password =@upd WHERE MentorID="+mentorid, conn);
                cmd.Parameters.AddWithValue("@upd", txtPass.Text);
              
                //  cmd.Parameters.AddWithValue("@mentorid", ddlMentor.SelectedIndex);
                //  cmd.Parameters.AddWithValue("@course", ddlMentor.SelectedIndex);
                conn.Open();
                cmd.ExecuteScalar();
                conn.Close();
                lblMessage.Text = "Password Changed successfully";
            }
            else {
                lblErrorMessage.Text = "Passowrd and Confirm passowrd must be same";
            }

        }

       

        

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}

