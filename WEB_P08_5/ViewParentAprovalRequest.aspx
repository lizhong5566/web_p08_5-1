﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin.Master" AutoEventWireup="true" CodeBehind="ViewParentAprovalRequest.aspx.cs" Inherits="WEB_P08_5.ViewParentAprovalRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <p>
        Replies</p>
    <p>
        <asp:GridView ID="gvReply" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" runat="server">
              <Columns>
            <asp:TemplateField HeaderText="Parent Name">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("ParentName") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Student Name">
                <ItemTemplate>
                    <asp:Label ID="LabelType1" runat="server" Text='<%# Eval("StudentName") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Date Created">
                <ItemTemplate>
                    <asp:Label ID="LabelType2" runat="server" Text='<%# Eval("DateCreated") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" Text="Approve Request" CommandArgument='<%#Eval("ViewingRequestID")%>' OnClick="Button1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
                  </Columns>
        </asp:GridView>
    </p>
</asp:Content>
