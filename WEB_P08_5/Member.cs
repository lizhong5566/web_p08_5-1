﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_P08_5
{
    public class Member
    {
        public int ProjectID { get; set; }
        public int studentID { get; set; }
        public string Role { get; set; }
        public string Reflection { get; set; }
    }
}