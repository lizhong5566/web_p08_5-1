﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ViewSuggestion : System.Web.UI.Page
    {
        int PID;
        int MID;
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Student")))
                {
                    PopulateGridView();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void PopulateGridView()
        {
            student stud = new student();
            int sid = stud.getstudentId(Session["LoginID"].ToString());
           
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                
                string query = "Select * from Suggestion where Status='N' AND StudentID=" + sid;
                SqlDataAdapter oCmd = new SqlDataAdapter(query, conn);
               
                oCmd.Fill(dtTbl);
            }
            gvProject.DataSource = dtTbl;
            gvProject.DataBind();
        }

        protected void gvProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(btn.CommandArgument);
            string strConn = ConfigurationManager.ConnectionStrings
                  ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    (" UPDATE Suggestion SET Status =@upd WHERE SuggestionID=" + id, conn);
            cmd.Parameters.AddWithValue("@upd", "Y");

            //  cmd.Parameters.AddWithValue("@mentorid", ddlMentor.SelectedIndex);
            //  cmd.Parameters.AddWithValue("@course", ddlMentor.SelectedIndex);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            lblMessage.Text = "Suggestion Acknowledged successfull";
        }
        }
    }