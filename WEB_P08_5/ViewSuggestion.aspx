﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="ViewSuggestion.aspx.cs" Inherits="WEB_P08_5.ViewSuggestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <p>
        View Suggestions</p>
    <p>
      <asp:Label ID="lblMessage" runat="server" ForeColor="Red" ></asp:Label> 
        <asp:GridView ID="gvProject" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="SuggestionID"  OnSelectedIndexChanged="gvProject_SelectedIndexChanged">
        <Columns>
           
            <asp:TemplateField HeaderText="Suggestion">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date">
                <ItemTemplate>
                   <asp:Label ID="LabelName" runat="server" Text='<%# Eval("DateCreated") %>'></asp:Label>
                </ItemTemplate>
            <EditItemTemplate>
        <asp:TextBox ID="txt_Name" runat="server" Text='<%# Eval("DateCreated") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>

             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" Text=" Acknowledge " CommandArgument='<%#Eval("SuggestionID")%>' OnClick="Button1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
           
        </Columns>
    </asp:GridView>

    </p>
</asp:Content>
