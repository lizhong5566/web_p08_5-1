﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Parent.Master" AutoEventWireup="true" CodeBehind="ViewChildPortfolio.aspx.cs" Inherits="WEB_P08_5.ViewChildPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <p>
        Replies</p>
    <p>
       
        <asp:GridView ID="gvProject" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ProjectID"  OnSelectedIndexChanged="gvProject_SelectedIndexChanged">
        <Columns>
           
            <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_Type" runat="server" Text='<%# Eval("Title") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                   <asp:Label ID="LabelName" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                </ItemTemplate>
            <EditItemTemplate>
        <asp:TextBox ID="txt_Name" runat="server" Text='<%# Eval("Description") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="ProjectURL">
    <ItemTemplate>
        <asp:Label ID="LabelImage" runat="server" Text='<%# Eval("ProjectURL") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox ID="txt_Image" runat="server" Text='<%# Eval("ProjectURL") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>
            
        </Columns>
    </asp:GridView>

    </p>
</asp:Content>
