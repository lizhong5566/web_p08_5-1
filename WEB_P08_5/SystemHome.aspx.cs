﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_P08_5
{
    public partial class SystemHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("passAdmin")))
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

        }
    }
}