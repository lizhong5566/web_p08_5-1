﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ParentRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
             

            }          
        }

        //private void displayitemsetList() //display on grid view
        //{
        //    string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        //    SqlConnection conn = new SqlConnection(strConn);
        //    SqlCommand cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet", conn);
        //    SqlDataAdapter daSkillSet = new SqlDataAdapter(cmd);
        //    DataSet result = new DataSet();
        //    conn.Open();
        //    daSkillSet.Fill(result, "SkillSet");
        //    conn.Close();
        //    gv_skillset.DataSource = result;
        //    gv_skillset.DataBind();     
      
        //}

       

    

        protected void btnCreate_Click(object sender, EventArgs e)
        {
           
            
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("INSERT INTO Parent VALUES(@name, @emailaddr, @password) ", conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text);
            cmd.Parameters.AddWithValue("@emailaddr", txtEmailAddr.Text);
            cmd.Parameters.AddWithValue("@password", password.Text);
    
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
           
        }

        protected void cuvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                student Email = new student();

                if (Email.isEmailExist(txtEmailAddr.Text) == true)
                {
                    lblMessage.Text = "";
                    args.IsValid = false;
                }
                else
                {
                    lblMessage.Text = "Account Created!";
                    args.IsValid = true;
                }

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtEmailAddr.Text = string.Empty;
                txtName.Text = string.Empty;
            }
        }
    }
}

