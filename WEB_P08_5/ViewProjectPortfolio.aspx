﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="ViewProjectPortfolio.aspx.cs" Inherits="WEB_P08_5.ViewProjectPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <p>
        View Prioject Portfolio</p>
    <p>
       
        <asp:GridView ID="gvProject" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ProjectID"  OnSelectedIndexChanged="gvProject_SelectedIndexChanged">
        <Columns>
           
            <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label ID="LabelType1" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_Type" runat="server" Text='<%# Eval("Title") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                   <asp:Label ID="LabelName2" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                </ItemTemplate>
            <EditItemTemplate>
        <asp:TextBox ID="txt_Name" runat="server" Text='<%# Eval("Description") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>

            <asp:TemplateField HeaderText="Picture">
    <ItemTemplate>
        <img width="100" height="100"  src=<%# string.Format("/Upload/{0}",Eval("ProjectPoster"))%> />
         
       
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox ID="txt_Image" runat="server" Text='<%# Eval("ProjectPoster") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="ProjectURL">
    <ItemTemplate>
        <asp:Label ID="LabelImage" runat="server" Text='<%# Eval("ProjectURL") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox ID="txt_Image" runat="server" Text='<%# Eval("ProjectURL") %>'></asp:TextBox>
    </EditItemTemplate>
</asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" Text="Update" CommandArgument='<%#Eval("ProjectID")%>' OnClick="Button1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>

    </p>
</asp:Content>
