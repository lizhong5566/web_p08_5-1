﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="ProposedEPortfolio.aspx.cs" Inherits="WEB_P08_5.Mentor.ProposedEPortfolio" %>

<div style="text-align:right"> 
    <a href="ParentRegistration.aspx">Register As a Parent</a>
                        ||
                        <a href="Login.aspx">Login</a>
</div>

<form runat="server" id="f1">
    <p>
        <strong>E-Portfolios :&nbsp;&nbsp;&nbsp; </strong>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:GridView ID="gvStudent" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
               
                <asp:BoundField DataField="Name" HeaderText="Student Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </p>
    </form>

