﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_P08_5.Mentor
{
    public partial class EditStudent : System.Web.UI.Page
    {
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //ddlStatus.Items.Add("--Select--");
                //ddlStatus.Items.Add("Y");
                //ddlStatus.Items.Add("N");
                PopulateGridView();
                if (Request.QueryString["studentid"] != null)
                {
                    Student objStudent = new Student();

                    objStudent.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

                    int errorCode = objStudent.getDetails();
                    if (errorCode == 0)
                    {
                        lblName.Text = objStudent.name;
                        lblid.Text =Convert.ToString(objStudent.studentId);
                        LabCourse.Text = objStudent.Course;
                        LabDescription.Text = objStudent.description;
                        LabPhoto.Text = objStudent.Photo;
                        LabAchievement.Text = objStudent.achievement;
                        LabExternalLink.Text = objStudent.ExternalLink;
                        
                    }
                    else if (errorCode == -2)
                    {
                        lblMessage.Text = "Unable to retrieve student details for ID" + objStudent.studentId;
                    }
                }
            }
            
        }
        void PopulateGridView()
        {
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                student stud = new student();
                int sid = Convert.ToInt32(Request.QueryString["studentid"]);

                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("SELECT * from ProjectMember where StudentID=" + sid, conn);

                oCmd.Fill(dtTbl);
            }
            gvProject.DataSource = dtTbl;
            gvProject.DataBind();
        }
        protected void btnCreate_Click1(object sender, EventArgs e)
        {
            Mentors obj = new Mentors();
            int sid = Convert.ToInt32(Request.QueryString["studentid"]);
            int mentorid = obj.getmentorId(Session["LoginID"].ToString());
            DateTime dt = DateTime.Now;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("insert into Suggestion values(@mentorid,@Student,@sug,@status,@dat)", conn);
            cmd.Parameters.AddWithValue("@Student", sid);
            cmd.Parameters.AddWithValue("@sug", txtEmailAddr.Text);
            cmd.Parameters.AddWithValue("@mentorid", mentorid);
            cmd.Parameters.AddWithValue("@dat", dt);
            cmd.Parameters.AddWithValue("@status", "Y");
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            Label2.Text = "suggestion posted Successfully" /* + ddlStudent.SelectedItem.Text*/;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
          //  if (Page.IsValid)
           // {
                Student objStudent = new Student();

                objStudent.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

                //if (ddlStatus.SelectedIndex != 0)
                //{
                //    objStudent.status = ddlStatus.SelectedValue;
                //}
                objStudent.status ="Y";
                int errorCode = objStudent.update();

                if (errorCode == 0)
                {
                    lblMessage.Text = "Status has been updated successfully.";
                }
                else if (errorCode == -2)
                {
                    lblMessage.Text = "Unable to save status as student is not found.";
                }
            }
        }
    }
//}