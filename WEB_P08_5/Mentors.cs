﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public class Mentors
    {
        public string name { get; set; }
        public string Email { get; set; }
        public int mentorID { get; set; }
        public string dpass { get; set; }
        public int mid;
        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand
                       ("INSERT INTO Mentor ([Name], EmailAddr) " + "OUTPUT INSERTED.MentorID " +  "VALUES(@name, @Email) ", conn);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@Email", Email);
            conn.Open();
            int id = (int)cmd.ExecuteScalar();
            conn.Close();
            return id;

        }

        public int getEmail(ref DataSet result)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                              ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Mentor ", conn);
            SqlDataAdapter daEmailAddr = new SqlDataAdapter(cmd);
            conn.Open();
            daEmailAddr.Fill(result, "Mentor");
            conn.Close();
            return 0;
        }

        public bool isEmailExist(string Email)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                       ("SELECT * FROM Mentor WHERE EmailAddr=@selectedEmailAddr", conn);

            cmd.Parameters.AddWithValue("@selectedEmailAddr", Email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daEmail.Fill(result, "EmailDetails");
            conn.Close();

            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; // The Email exist           
            else
                return false; // The Email given does not exist

        }
        public int getDetails()
        {
            return 0;
        }
        public int getmentorId(string usename)
        {

            string strConn = ConfigurationManager.ConnectionStrings
                             ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand
                ("SELECT MentorID FROM Mentor WHERE EmailAddr=@uname", conn);

            cmd.Parameters.AddWithValue("@uname", usename);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            // using (SqlDataReader dr = cmd.ExecuteReader())
            if (dr.Read())
            {
                mid = (int)dr["MentorID"];
            }
            return mid;
        }
    }
}