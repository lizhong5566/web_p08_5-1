﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public class Skillset
    {
        public int StudentID { get; set; }
        public string SkillSetName { get; set; }
        public int SkillSetID { get; set; }

        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings
                            ["Student_EPortfolio"].ToString();


            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("INSERT INTO SkillSet (SkillSetName) " + "OUTPUT INSERTED.SkillSetID " + "VALUES(@skillset) ", conn);
            cmd.Parameters.AddWithValue("@skillset", SkillSetName);
            conn.Open();
            int id = (int)cmd.ExecuteScalar();
            conn.Close();
            return id;


        }

        public int getSkillSet(ref DataSet result)
        {
            //    Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings
                              ["Student_EPortfolio"].ToString();

            //    Instantiate a SQLCommand object, supply the connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //    Instantiate a SQLCommand object, supply it with a SELECT SQL
            //    Statement which retrieve all atrributes of a skillset record.
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM SkillSet ", conn);


            //    Instantiate a DataAdapter object, pass the SQLCommand
            //    Object created as parameter.
            SqlDataAdapter daSkillSet = new SqlDataAdapter(cmd);

            //    Open a database connection 
            conn.Open();

            //    Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            daSkillSet.Fill(result, "SkillSet");

            //    Close a database connection
            conn.Close();

            //    Return 0 when no error occurs. 
            return 0;

        }

        public bool isSkillsetExist(string skillSetName)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                       ("SELECT * FROM SkillSet WHERE SkillSetName=@selectedSkillSet", conn);

            cmd.Parameters.AddWithValue("@selectedSkillSet", skillSetName);

            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daSkill.Fill(result, "SkillSet");
            conn.Close();

            if (result.Tables["SkillSet"].Rows.Count > 0)
                return true; // The skill exist           
            else
                return false; // The skill given does not exist
        }
    }
}
