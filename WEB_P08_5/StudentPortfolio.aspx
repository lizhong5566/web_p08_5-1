﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentPortfolio.aspx.cs" Inherits="WEB_P08_5.StudentPortfolio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
              <tr>
            <td class="auto-style4">Title : </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Description :</td>
            <td class="auto-style3">
               
                <asp:TextBox ID="txtEmailAddr" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="Please enter email address" ForeColor="Red"></asp:RequiredFieldValidator>
               
            </td>
        </tr>

            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
              &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
         
        </div>
    </form>
</body>
</html>
