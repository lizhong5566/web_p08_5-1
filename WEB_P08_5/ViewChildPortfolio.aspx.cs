﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ViewChildPortfolio : System.Web.UI.Page
    {
       
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Parent")))
                {
                    PopulateGridView();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void PopulateGridView()
        {
            student stud = new student();

            int Pid = stud.getParentId(Session["LoginID"].ToString());
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("SELECT Project.ProjectID, Project.Title, Project.Description, Project.ProjectPoster, Project.ProjectURL FROM Project INNER JOIN ProjectMember ON ProjectMember.ProjectID = Project.ProjectID Inner Join ViewingRequest on ProjectMember.StudentID = ViewingRequest.StudentID where ViewingRequest.ParentID="+ Pid, conn);
                oCmd.Fill(dtTbl);
            }
            gvProject.DataSource = dtTbl;
            gvProject.DataBind();
        }

        protected void gvProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
        }
        }
    }