﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WEB_P08_5.Login" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Student Portfolio Login</title>
    <meta charset="utf-8" />  
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>                
                <div class="col-md-4" id="form-div">
                    <div class="form-login">
                    <div class="form-group">
                        <asp:Label ID="lblUsr" runat="server" Text="Username:" AssociatedControlID="txtUsername"></asp:Label>
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPass" runat="server" Text="Password:" AssociatedControlID="txtPassword"></asp:Label>
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text="*"></asp:Label>
                        <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:Label ID="lblLoginAs" runat="server" Text="Login As:" ></asp:Label> 
                        <br />
                        <br />
                        <asp:RadioButton  ID="radioSysAdmin" runat="server" Checked="True" GroupName="Users" Text="System Admin" />
                        <br />
                        <asp:RadioButton  ID="radioStudent" runat="server" GroupName="Users" Text="Student" /> <br />
                        <asp:RadioButton  ID="radioMentor" runat="server" GroupName="Users" Text="Mentor" />
                        <br />
                        <asp:RadioButton  ID="radioParent" runat="server" GroupName="Users" Text="Parent" />
                    </div>
                    <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-block btn-primary" OnClick="btnLogin_Click" />
                    <br />
                        <a href="ParentRegistration.aspx">Register As a Parent</a>
                        <br />
                        <a href="ProposedEPortfolio.aspx">Proposed E-Portfolio</a>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </form>
</body>
</html>

