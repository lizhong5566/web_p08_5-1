﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5.Mentor
{
    public partial class ProposedEPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                    displayStudentList();
            
        }
        }

        private void displayStudentList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();
            daStudent.Fill(result, "StudentDetails");
            conn.Close();

            gvStudent.DataSource = result.Tables["StudentDetails"];
            gvStudent.DataBind();

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
              //  lblMessage.Text = result.Tables["StudentDetails"].Rows.Count.ToString() + " Portfolios";
            }
            else
            {
                lblMessage.Text = "No student record";
            }
        }

    }
}