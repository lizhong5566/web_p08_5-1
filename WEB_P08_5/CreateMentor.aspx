﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin.Master" AutoEventWireup="true" CodeBehind="CreateMentor.aspx.cs" Inherits="WEB_P08_5.CreateMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 240px;
    }
    .auto-style2 {
        width: 240px;
        height: 29px;
    }
    .auto-style3 {
        height: 29px;
    }
        .auto-style4 {
            width: 240px;
            height: 33px;
        }
        .auto-style5 {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="w-100">
    <tr>
        <td class="auto-style2"> </td>
        <td class="auto-style3">
        </td>
    </tr>
    <tr>
        <td class="auto-style1">Name :</td>
        <td>
            <asp:TextBox ID="txtName" runat="server" Height="27px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style4">Email Address :</td>
        <td class="auto-style5">
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter email" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td>
            <asp:CustomValidator ID="cuvEmail" runat="server" ErrorMessage="Email Already Exist" OnServerValidate="cuvEmail_ServerValidate" ForeColor="Red"></asp:CustomValidator>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </td>
    </tr>

    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td>
            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
</asp:Content>
