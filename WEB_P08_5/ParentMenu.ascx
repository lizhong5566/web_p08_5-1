﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParentMenu.ascx.cs" Inherits="WEB_P08_5.ParentMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <br />
<a class="navbar-brand" href="ViewChildPortfolio.aspx"
    style="font-size:32px; font-weight:bold; color:#3399FF;">
    Parent
    </a>

<button class="navbar-toggler" type="button"
    data-toggle="collapse" data-target="#staffNavbar">
    <span class="navbar-toggler-icon">
        </span>
</button>

<div class="collapse navbar-collapse" id="staffNavbar">
    <ul class="navbar-nav mr-auto">
        
        <li class="nav-item">
            
            <a class="nav-link" href="RequestPortfolioView.aspx">Submit Viewing Request</a>
        </li>
                <li class="nav-item">
            <a class="nav-link" href="ViewChildPortfolio.aspx">View Child Portfolio</a>
        </li>
        
                <li class="nav-item">
            <a class="nav-link" href="PostMessage.aspx">Post Message To Mentor</a>
        </li>
       
                <li class="nav-item">
            <a class="nav-link" href="ViewReply.aspx">View Replies</a>
        </li>
        </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">

            <asp:Button ID="btnLogOut" runat="server" Text="Log out" CssClass="btn btnlink nav-link" CausesValidation="false" OnClick="btnLogOut_Click"/>
            </li>
            </ul>
</div>

</nav>