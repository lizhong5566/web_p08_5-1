﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_P08_5
{
    public class Project
    {
        public int ProjectID { get; set; }
        public string title { get; set; }
        public string Description { get; set; }
        public List<student> Members { get; set; }
    }
}