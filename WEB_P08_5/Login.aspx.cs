﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Session["LoginID"] as string))
            {
                if (Session["UserType"].Equals("Parent"))
                {
                    Response.Redirect("ViewChildPortfolio.aspx");
                }
                else if (Session["UserType"].Equals("Student"))
                {
                    Response.Redirect("MyPersonalProfile.aspx");
                }
                else if (Session["UserType"].Equals("SystemAdmin"))
                {
                    Response.Redirect("SystemHome.aspx");
                }
                else
                {
                    Response.Redirect("ApprovePortfolio.aspx");
                }

            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string userType = "";
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            string strConn = ConfigurationManager.ConnectionStrings
                              ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand
                ("SELECT EmailAddr, Password FROM Student WHERE EmailAddr = @uname AND Password = @pw", conn);

            cmd.Parameters.AddWithValue("@uname", txtUsername.Text);
            cmd.Parameters.AddWithValue("@pw", txtPassword.Text);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                if (radioStudent.Checked == true)
                {
                    userType = "Student";
                    SetSession(txtUsername.Text,"Student");
                    Response.Redirect("MyPersonalProfile.aspx");                    
                }
            }
            else
            {
                lblMessage.Text = "Invalid Login Credentials";
            }

            SqlCommand cmd1 = new SqlCommand
              ("SELECT EmailAddr, Password FROM Mentor WHERE EmailAddr = @uname1 AND Password = @pw1", conn);

            cmd1.Parameters.AddWithValue("@uname1", txtUsername.Text);
            cmd1.Parameters.AddWithValue("@pw1", txtPassword.Text);

            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                if (radioMentor.Checked == true)
                    userType = "Mentor";
                SetSession(txtUsername.Text, "Mentor");  
                Response.Redirect("ApprovePortfolio.aspx");
            }
            else
            {
                lblMessage.Text = "Invalid Login Credentials";
            }
            SqlCommand cmd2 = new SqlCommand
              ("SELECT EmailAddr, Password FROM Parent WHERE EmailAddr = @uname2 AND Password = @pw2", conn);

            cmd2.Parameters.AddWithValue("@uname2", txtUsername.Text);
            cmd2.Parameters.AddWithValue("@pw2", txtPassword.Text);

            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            if (dt2.Rows.Count > 0)
            {
                if (radioParent.Checked == true)
                userType = "Parent";
                SetSession(txtUsername.Text, "Parent");
                Response.Redirect("ViewChildPortfolio.aspx");
            }
            else
            {
                lblMessage.Text = "Invalid Login Credentials";
            }

            if (radioSysAdmin.Checked == true)
                userType = "SystemAdmin";

            if (username == "admin@ap.edu.sg" && password == "passAdmin" && userType == "SystemAdmin")
            {
                SetSession(username, password);
                Response.Redirect("SystemHome.aspx");
            }
            else
            {
                //lblErrorMessage.Text = "Invalid Login Credentials!";
            }
            conn.Close();
        }
           
        
        protected void SetSession(string username,string userType)
        {
            Session["LoginID"] = username;
            Session["UserType"] = userType;
            Session["LoggedInTime"] = DateTime.Now.ToString();
        }

    }
}