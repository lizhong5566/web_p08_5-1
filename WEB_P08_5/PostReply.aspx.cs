﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class PostReply : System.Web.UI.Page
    {
        int PID;
        int MID;
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {

        }





        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            Mentors obj = new Mentors();

            int mentorid = obj.getmentorId(Session["LoginID"].ToString());
            int messageid =Convert.ToInt32(Request.QueryString["msg"]);
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                SqlCommand oCmd = new SqlCommand("Select FromID from Message where MessageID=@messageid", conn);
                oCmd.Parameters.AddWithValue("@messageid", messageid);
                conn.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        PID = (int)oReader["FromID"];
                    }
                    conn.Close();
                }



                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Reply VALUES(@messageid, @mtrid, @Pid, @dtp, @text) ";
                    cmd.Parameters.AddWithValue("@messageid", messageid);
                    cmd.Parameters.AddWithValue("@mtrid", mentorid);
                    cmd.Parameters.AddWithValue("@Pid", PID);
                    cmd.Parameters.AddWithValue("@dtp", DateTime.Now);

                    cmd.Parameters.AddWithValue("@text", tbParentMsg.Text);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    lblMessage.Text = "Reply sent successfully";
                }
            }
        }
    }
}