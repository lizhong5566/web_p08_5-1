﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="UpdateProjectPortfolio.aspx.cs" Inherits="WEB_P08_5.UpdateProjectPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 551px;
        }
        .auto-style2 {
            width: 551px;
            height: 25px;
        }
        .auto-style3 {
            height: 25px;
        }
        .auto-style4 {
            width: 551px;
            height: 29px;
        }
        .auto-style5 {
            height: 29px;
        }
        .auto-style6 {
            width: 551px;
            height: 30px;
        }
        .auto-style7 {
            height: 30px;
        }
        .auto-style8 {
            width: 551px;
            height: 26px;
        }
        .auto-style9 {
            height: 26px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder2">
    <table class="w-100">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">Title : </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Description :</td>
            <td class="auto-style3">
               
                <asp:TextBox ID="txtEmailAddr" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="Please enter email address" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
      
        <%-- <tr>
            <td class="auto-style2">Reflection :</td>
            <td class="auto-style3">
               
                <asp:TextBox ID="txtReflection" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReflection" Display="Dynamic" ErrorMessage="Please enter email address" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>--%>
          <tr>
            <td class="auto-style4">Project URL : </td>
            <td class="auto-style5">
                <asp:TextBox ID="ProjectURL" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ProjectURL" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td class="auto-style1">Photo</td>
            <td>
               
                <asp:FileUpload ID="FileUpload1" runat="server" />
               
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnCreate" runat="server" Text="Update" CommandArgument='<%#Eval("ProjectID")%>' OnClick="btn_update_Click" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
