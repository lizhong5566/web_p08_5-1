﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class Skillset1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("passAdmin")))
                {
                }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Skillset skills = new Skillset();
                skills.SkillSetName = txtSkillSet.Text;
                int id = skills.add();
                
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSkillSet.Text = string.Empty;
        }

        protected void cuvSkillSet_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                Skillset Skills = new Skillset();

                if (Skills.isSkillsetExist(txtSkillSet.Text) == true)
                {
                    args.IsValid = false;
                    
                }
                else
                    args.IsValid = true;
               
            }
        }
    }
}