﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class SearchStudent : System.Web.UI.Page
    {
        
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e) {


            if (!IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Mentor")))
                {
                    displayStudentDropDownList();
                    displayStudentDropDownList1();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }


        }
        
        private void displayStudentDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [SkillSetID],[SkillSetName] FROM SkillSet", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "SkillSet");
            conn.Close();
            ddlSKill.DataSource = result.Tables["SkillSet"];
            ddlSKill.DataTextField = "SkillSetName";
            ddlSKill.DataValueField = "SkillSetID";
            ddlSKill.DataBind();
            ddlSKill.Items.Insert(0, "--Select Skill--");
        }
        private void displayStudentDropDownList1()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [StudentID],[Name] FROM Student", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Student");
            conn.Close();
            ddlStudent.DataSource = result.Tables["Student"];
            ddlStudent.DataTextField = "Name";
            ddlStudent.DataValueField = "StudentID";
            ddlStudent.DataBind();
            ddlStudent.Items.Insert(0, "--Select Student--");
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {

            int skil = ddlSKill.SelectedIndex;
            int stuid = ddlStudent.SelectedIndex;
            if (stuid > 0)
            {
                PopulateGridView1(stuid);
            }
            else if (skil > 0)
            {
                PopulateGridView(skil);
            }
           
            else
            {
                lblMessage.Text = "Select Skill or student";
            }
        }
        void PopulateGridView(int skl)
        {
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("Select Student.Name,Student.Course,Student.Description from Student inner Join StudentSkillSet on Student.StudentID = StudentSkillSet.StudentID where StudentSkillSet.SkillSetID=" + skl, conn);
                oCmd.Fill(dtTbl);
            }
            gvReply.DataSource = dtTbl;
            gvReply.DataBind();
        }
        void PopulateGridView1(int sid)
        {
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("Select Name,Course,Description from Student where StudentID=" + sid, conn);
                oCmd.Fill(dtTbl);
            }
            gvReply.DataSource = dtTbl;
            gvReply.DataBind();
        }
    }
}