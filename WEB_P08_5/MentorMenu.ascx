﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MentorMenu.ascx.cs" Inherits="WEB_P08_5.Mentor.MentorMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <a class="navbar-brand" href="ApprovePortfolio.aspx"
        style="font-size:32px; font-weight:bold; color:#3399FF;">
        Mentor
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mentorNavbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="ApprovePortfolio.aspx">Approve Portfolios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="CreateSuggestion.aspx">Post Suggestions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="SearchStudent.aspx">Search Students</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ViewMessages.aspx">View Messages</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ChangePassword.aspx">Change Password</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out" CssClass="btn btn-link nav-link" CausesValidation="false" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
</nav>