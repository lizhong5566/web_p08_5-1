﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class MyPersonalProfile : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            displayitemsetList();
            display();


        }
        private void displayitemsetList()
        {
            string email = (String)Session["LoginID"];

            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //create a student object
            student objStudent = new student();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instatiate a SQLcommand object, supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database.

            //SqlCommand cmd = new SqlCommand("Select Achievement, Description from Student" +
            //                                " Where EmailAddr = @email", conn); //i use email to verify 
            SqlCommand cmd = new SqlCommand("Select Achievement, Description from Student where EmailAddr = @email", conn);



            //cmd.Parameters.AddWithValue("@email", @email);

            if (string.IsNullOrEmpty(email))
            {
                cmd.Parameters.AddWithValue("@email", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@email", email);
            }

             
            //Declare and instantiate DataAdapater object
            SqlDataAdapter daSkillSet = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database 
            DataSet result = new DataSet();

            conn.Open();

            //Use DataAdapter to fetch data to a table "Skillset" in DataSet.
            daSkillSet.Fill(result, "Student");
            conn.Close();

            //display the list of data in gridview
            gv_profile.DataSource = result.Tables["Student"];
            gv_profile.DataBind();
       //     lblname.Text = result.Tables["Description"].ToString();

        }

        public void display()
        {
            string email = (String)Session["LoginID"];

            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                string display = "Select * from Student Where EmailAddr = @email";
                SqlCommand cmd = new SqlCommand(display,conn);
                cmd.Parameters.AddWithValue("@email",email);

                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lblname.Text = reader["Name"].ToString();
                        lblachievement.Text = reader["Achievement"].ToString();
                        lblaboutme.Text = reader["Description"].ToString();
                    }
                }
            }



        }

        protected void gv_profile_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("UpdatePersonalProfile.aspx");

        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {

            Response.Redirect("UpdatePersonalProfile.aspx?achievement=" + lblachievement.Text + "&Description=" + lblaboutme.Text);
        }
    }

}
    
