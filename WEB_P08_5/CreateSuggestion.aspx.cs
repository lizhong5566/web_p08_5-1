﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class CreateSuggestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Mentor")))
                {
                    displayStudentDropDownList();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void displayStudentDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [StudentID],[Name] FROM Student", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Student");
            conn.Close();
            ddlStudent.DataSource = result.Tables["Student"];
            ddlStudent.DataTextField = "Name";
            ddlStudent.DataValueField = "StudentID";
            ddlStudent.DataBind();
            ddlStudent.Items.Insert(0, "--Select Student--");
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Mentors obj = new Mentors();
            
            int mentorid = obj.getmentorId(Session["LoginID"].ToString()); 
            DateTime dt = DateTime.Now;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("insert into Suggestion values(@mentorid,@Student,@sug,@status,@dat)", conn);
            cmd.Parameters.AddWithValue("@Student", ddlStudent.SelectedIndex);
            cmd.Parameters.AddWithValue("@sug", txtEmailAddr.Text);
           cmd.Parameters.AddWithValue("@mentorid", mentorid);
            cmd.Parameters.AddWithValue("@dat", dt);
            cmd.Parameters.AddWithValue("@status","Y");
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            lblMessage.Text = "Successfully posted suggestion to"+ ddlStudent.SelectedItem.Text;
        }

       

      

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}

