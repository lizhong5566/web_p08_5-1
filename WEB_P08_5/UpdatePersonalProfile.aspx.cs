﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_P08_5
{
	public partial class UpdatePersonalProfile : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!Page.IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Student")))
                {

                    string email = (String)Session["LoginID"];
                    txt_achievements.Text = Request.QueryString["achievement"];
                    txt_selfdescription.Text = Request.QueryString["Description"];
                    lbl_email.Text = email;
                    displayskillsetDropDownList();

                    if (Request.QueryString["StudentID"] != null)
                    {
                        student objstudent = new student();

                        objstudent.studentID = Convert.ToInt32(Request.QueryString["StudentID"]);

                        //load student information to controls
                        int errorCode = objstudent.getDetails();
                        if (errorCode == 0)
                        {
                            lbl_email.Text = objstudent.email;
                        }
                    }
                }
                else {
                    Response.Redirect("Login.aspx");
                }
            }
		}

        protected void btn_update_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
                using (SqlConnection conn = new SqlConnection(strConn))
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Student SET Achievement = @achievement, description = @description, EmailAddr=@email " + "WHERE EmailAddr = @previousemail ";
                        
                    cmd.Parameters.AddWithValue("@achievement",txt_achievements.Text);
                    cmd.Parameters.AddWithValue("@description",txt_selfdescription.Text);
                    cmd.Parameters.AddWithValue("@email", txt_newemail.Text);
                    cmd.Parameters.AddWithValue("@previousemail", lbl_email.Text);
                    conn.Open();
                    cmd.ExecuteScalar();
                    conn.Close();
                }
                Session["LoginID"] = txt_newemail.Text;
                lblMessage.Text = "Information updated Successfully";
                Response.Redirect("MyPersonalProfile.aspx");
            }
        }

        protected void txt_achievements_TextChanged(object sender, EventArgs e)
        {

        }
        private void displayskillsetDropDownList()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantiate a sqlconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);


            //Instantiate a sqlcommand object, supply it with a select sql
            //statement with retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet", conn);

            //create a DataAdapter object
            SqlDataAdapter daskillset = new SqlDataAdapter(cmd);

            //Create DataSet object
            DataSet result = new DataSet();

            //open a database connection
            conn.Open();
            //use DataAdapter to fetch data to a table "BranchDetails" in dataset
            daskillset.Fill(result, "SkillSet");
            //close database connection
            conn.Close();

            //specify the dropdown list to get data from the dataset
            ddlskillset.DataSource = result.Tables["SkillSet"];

            //specify the text property of dropdownlist
            ddlskillset.DataTextField = "SkillSetName";

            //specify the value property of dropdownlist
            ddlskillset.DataValueField = "SkillSetID";

            // Load branch information to the drop-down list
            ddlskillset.DataBind();

            //Insert prompt for the dropdownList at the first position of the list
            ddlskillset.Items.Insert(0, "--Select--");


        }

        protected void ddlskillset_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}