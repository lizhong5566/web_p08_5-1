﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ViewReply : System.Web.UI.Page
    {
        string username = "Peter_Tan@yahoo.com";
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e) {

          
            if (!IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Parent")))
                {
                    PopulateGridView();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }


            }
        void PopulateGridView()
        {
            student stud = new student();

            int Pid = stud.getParentId(Session["LoginID"].ToString());
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("SELECT Reply.Text,Reply.DateTimePosted,Mentor.Name As MenterName from Reply inner join Mentor on Mentor.MentorID = Reply.MentorID where ParentId="+ Pid, conn);
                oCmd.Fill(dtTbl);
            }
            gvReply.DataSource = dtTbl;
            gvReply.DataBind();
        }
       
    }
}