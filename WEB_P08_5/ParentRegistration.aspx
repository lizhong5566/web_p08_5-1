﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="ParentRegistration.aspx.cs" Inherits="WEB_P08_5.ParentRegistration" %>

    <style type="text/css">
        .auto-style1 {
            width: 551px;
        }
        .auto-style2 {
            width: 551px;
            height: 25px;
        }
        .auto-style3 {
            height: 25px;
        }
        .auto-style4 {
            width: 551px;
            height: 29px;
        }
        .auto-style5 {
            height: 29px;
        }
        .auto-style6 {
            width: 551px;
            height: 30px;
        }
        .auto-style7 {
            height: 30px;
        }
        .auto-style8 {
            width: 551px;
            height: 26px;
        }
        .auto-style9 {
            height: 26px;
        }
    </style>

<div style="text-align:right"> 
    <a href="ProposedEPortfolio.aspx">Home Page</a>
                        ||
                        <a href="Login.aspx">Login</a>
</div>
<form runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">Name : </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Email Address :</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtEmailAddr" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="Please enter email address" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cuvEmail" runat="server" Display="Dynamic" ErrorMessage="Email Already Exist" ForeColor="Red" OnServerValidate="cuvEmail_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
   
      <tr>
            <td class="auto-style4">Password : </td>
            <td class="auto-style5">
                <asp:TextBox ID="password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="password" Display="Dynamic" ErrorMessage="Please enter name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
      
        <tr>
            <td class="auto-style8"></td>
            <td class="auto-style9">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>

</form>

