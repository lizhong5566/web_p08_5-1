﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class AddMembers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Student")))
                {
                    displayProjectDropDownList();
                displayStudentDropDownList();

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void displayProjectDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [ProjectID],[Title] FROM Project ", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Project");
            conn.Close();
            ddlProject.DataSource = result.Tables["Project"];
            ddlProject.DataTextField = "Title";
            ddlProject.DataValueField = "ProjectID";
            ddlProject.DataBind();
            ddlProject.Items.Insert(0, "--Select Project--");
        }

        private void displayStudentDropDownList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT [StudentID],[Name] FROM Student", conn);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMentor.Fill(result, "Student");
            conn.Close();
            ddlStudent.DataSource = result.Tables["Student"];
            ddlStudent.DataTextField = "Name";
            ddlStudent.DataValueField = "StudentID";
            ddlStudent.DataBind();
            ddlStudent.Items.Insert(0, "--Select Student--");
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Member objmemeber = new Member();

            objmemeber.ProjectID = Convert.ToInt32(ddlProject.SelectedValue);
            objmemeber.studentID = Convert.ToInt32(ddlStudent.SelectedValue);

            objmemeber.Reflection = txtReflection.Text;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
           
            SqlCommand cmd1 = new SqlCommand
                    ("insert into [ProjectMember] (ProjectID,StudentID,Role,Reflection) values(@name,@emailaddr,@role,@reflection)", conn);
            cmd1.Parameters.AddWithValue("@name", objmemeber.ProjectID);
            cmd1.Parameters.AddWithValue("@emailaddr", objmemeber.studentID);
            cmd1.Parameters.AddWithValue("@role", "Member");
            cmd1.Parameters.AddWithValue("@reflection", objmemeber.Reflection);
            conn.Open();
            cmd1.ExecuteScalar();
            conn.Close();
            lblMessage.Text = "Member Added Successfully";
        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //    if (Page.IsValid)
            //    {
            //        txtEmailAddr.Text = string.Empty;
            //        txtName.Text = string.Empty;
            //    }
        }

    protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}

