﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_P08_5
{
	public partial class UpdateProjectPortfolio : System.Web.UI.Page
	{
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e)
		{
            if (!Page.IsPostBack)
            {
             int ProjectID =Convert.ToInt32(Request.QueryString["Pid"]); 
                using (SqlConnection conn = new SqlConnection(strConn))
                {
                    //string oString = "Select ParentID from Parent where EmailAddr=@user";
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select * from Project where ProjectID=@id", conn);
                    cmd.Parameters.AddWithValue("@id", ProjectID);
                   
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            txtName.Text = reader["Title"].ToString();
                            txtEmailAddr.Text = reader["Description"].ToString();
                            //txtReflection.Text = reader["txtReflection"].ToString();
                            ProjectURL.Text= reader["ProjectURL"].ToString();

                        }
                    }
                }
            }
		}

        protected void btn_update_Click(object sender, EventArgs e)
        {
            // Button btn = (Button)sender;
            // int id = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(Request.QueryString["Pid"]);
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "/Upload/" + FileUpload1.FileName.ToString());
            Project objProj = new Project();
            
           
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("Update Project set Title=@Title, Description=@des,ProjectURL=@ProjUrl,ProjectPoster=@photo where ProjectID="+id, conn);
            cmd.Parameters.AddWithValue("@Title", txtName.Text);
            cmd.Parameters.AddWithValue("@des", txtEmailAddr.Text);
            cmd.Parameters.AddWithValue("@ProjUrl", ProjectURL.Text);
     
            cmd.Parameters.AddWithValue("@photo", FileUpload1.FileName);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
        }

        protected void txt_achievements_TextChanged(object sender, EventArgs e)
        {

        }
        private void displayskillsetDropDownList()
        {
            
        }

        protected void ddlskillset_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}