﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentMenu.ascx.cs" Inherits="WEB_P08_5.StudentMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <br />
<a class="navbar-brand" href="MyPersonalProfile.aspx"
    style="font-size:32px; font-weight:bold; color:#3399FF;">
    Student
    </a>

<button class="navbar-toggler" type="button"
    data-toggle="collapse" data-target="#staffNavbar">
    <span class="navbar-toggler-icon">
        </span>
</button>

<div class="collapse navbar-collapse" id="staffNavbar">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="UpdatePersonalProfile.aspx">Update Personal Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="CreatePortfolio.aspx">Create Project Portfolio</a>
        </li>
                <li class="nav-item">
            <a class="nav-link" href="MyPersonalProfile.aspx">Profile</a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="ViewSuggestion.aspx">View Suggestions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="ViewProjectPortfolio.aspx">View Portfolios</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="AddMembers.aspx">Add Member</a>
        </li>
        </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">

            <asp:Button ID="btnLogOut" runat="server" Text="Log out" CssClass="btn btnlink nav-link" CausesValidation="false" OnClick="btnLogOut_Click"/>
            </li>
            </ul>
</div>

</nav>