﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5.Mentor
{
    public class Student
    {
        public int studentId { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string achievement { get; set; }
        public string ExternalLink { get; set; }
        public string Photo { get; set; }
        public string Course { get; set; }
       

        public int getDetails()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @selectedStudentID", conn);
            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);

            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();
            daStudent.Fill(result, "StudentDetails");
            conn.Close();

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                DataTable table = result.Tables["StudentDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    name = table.Rows[0]["Name"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Status"]))
                    status = table.Rows[0]["Status"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                    achievement = table.Rows[0]["Achievement"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                    Course = table.Rows[0]["Course"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    Photo = table.Rows[0]["Photo"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                    ExternalLink = table.Rows[0]["ExternalLink"].ToString();

                return 0;
            }
            else
            {
                return -2;
            }
        }

        public int update()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = @status" +
                                            " WHERE StudentID = @selectedStudentID", conn);

            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);


            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            if (count > 0)
                return 0;
            else
                return -2;
        }
    }
}