﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor.Master" AutoEventWireup="true" CodeBehind="CreateSuggestion.aspx.cs" Inherits="WEB_P08_5.CreateSuggestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 551px;
        }
        .auto-style2 {
            width: 551px;
            height: 25px;
        }
        .auto-style3 {
            height: 25px;
        }
        .auto-style4 {
            width: 551px;
            height: 29px;
        }
        .auto-style5 {
            height: 29px;
        }
        .auto-style6 {
            width: 551px;
            height: 30px;
        }
        .auto-style7 {
            height: 30px;
        }
        .auto-style8 {
            width: 551px;
            height: 26px;
        }
        .auto-style9 {
            height: 26px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder2">
    <table class="w-100">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
        </tr>
       <tr>
            <td class="auto-style2">Select Student:</td>
            <td class="auto-style3">
               
               <asp:DropDownList ID="ddlStudent" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlStudent" Display="Dynamic" ErrorMessage="Please select Mentor" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
       <tr>
        <tr>
            <td class="auto-style2">Suggestion :</td>
            <td class="auto-style3">
               
                <asp:TextBox ID="txtEmailAddr" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" Display="Dynamic" ErrorMessage="Please enter email address" ForeColor="Red"></asp:RequiredFieldValidator>
             
            </td>
        </tr>
      
      <tr>
          <td>

          </td>
          <td>
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" ></asp:Label>
          </td>
      </tr>
        
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" />
         
            </td>
        </tr>
    </table>
</asp:Content>


