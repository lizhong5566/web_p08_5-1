﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor.Master" AutoEventWireup="true" CodeBehind="ViewMessages.aspx.cs" Inherits="WEB_P08_5.ViewMessages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <p>
        Replies</p>
    <p>
        <asp:GridView ID="gvReply" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="MessageID"  OnSelectedIndexChanged="gvProject_SelectedIndexChanged" runat="server">
            <Columns>
                <asp:TemplateField HeaderText="Parent Name">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("ParentName") %>'></asp:Label>
                </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label ID="LabelType3" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField HeaderText="Text">
                <ItemTemplate>
                    <asp:Label ID="LabelType1" runat="server" Text='<%# Eval("Text") %>'></asp:Label>
                </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Time Posted">
                <ItemTemplate>
                    <asp:Label ID="LabelType2" runat="server" Text='<%# Eval("DateTimePosted") %>'></asp:Label>
                </ItemTemplate>
                    </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" Text="Reply" CommandArgument='<%#Eval("MessageID")%>' OnClick="Button1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
                </Columns>
        </asp:GridView>
    </p>
</asp:Content>
