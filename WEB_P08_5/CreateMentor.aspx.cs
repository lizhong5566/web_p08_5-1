﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_P08_5
{
    public partial class CreateMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("passAdmin")))
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Mentors objMentor = new Mentors();
                string name = txtName.Text;
                objMentor.name = txtName.Text;
                string Email = txtEmail.Text;
                objMentor.Email = txtEmail.Text;
                int id = objMentor.add();
 
            }
        }

        protected void cuvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                Mentors Email = new Mentors();

                if (Email.isEmailExist(txtEmail.Text) == true)
                {
                    lblMessage.Text = "";
                    args.IsValid = false;
                    
                }
                else
                {
                    lblMessage.Text = "Account Created!";
                    args.IsValid = true;
                }
                    
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtEmail.Text = string.Empty;
                txtName.Text = string.Empty;
            }
        }
    }
}