﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class ViewMessages : System.Web.UI.Page
    {
        
        string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
        protected void Page_Load(object sender, EventArgs e) {

          
            if (!IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Mentor")))
                {
                    PopulateGridView();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }


            }
        void PopulateGridView()
        {
            Mentors obj = new Mentors();

            int mentorid = obj.getmentorId(Session["LoginID"].ToString());
            DataTable dtTbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                //string oString = "Select ParentID from Parent where EmailAddr=@user";
                conn.Open();
                SqlDataAdapter oCmd = new SqlDataAdapter("Select Parent.ParentName,Message.MessageID,Message.DateTimePosted,Message.Title,Message.Text from Message inner join Parent on Parent.ParentID = Message.FromID where Message.ToID="+mentorid, conn);
                oCmd.Fill(dtTbl);
            }
            gvReply.DataSource = dtTbl;
            gvReply.DataBind();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(btn.CommandArgument);
            Response.Redirect("PostReply.aspx?msg="+id);
        }
        protected void gvProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //int PID;
        //int MID;
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    string username = "Peter_Tan@yahoo.com";
        //    var strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

        //    using (SqlConnection conn = new SqlConnection(strConn))
        //    {
        //        //string oString = "Select ParentID from Parent where EmailAddr=@user";
        //        SqlCommand oCmd = new SqlCommand("Select ParentID from Parent where EmailAddr = @user", conn);
        //        oCmd.Parameters.AddWithValue("@user", username);
        //        conn.Open();
        //        using (SqlDataReader oReader = oCmd.ExecuteReader())
        //        {
        //            while (oReader.Read())
        //            {
        //                PID = (int)oReader["ParentID"];
        //            }
        //            conn.Close();
        //        }
        //    } //PID
        //    using (SqlConnection conn = new SqlConnection(strConn))
        //    {
        //        string oString = "Select Name, MentorID from Mentor";
        //        SqlCommand oCmd = new SqlCommand(oString, conn);
        //        conn.Open();
        //        using (SqlDataReader oReader = oCmd.ExecuteReader())
        //        {
        //            while (oReader.Read())
        //            {
        //                MID = (int)oReader["MentorID"];
        //            }
        //            conn.Close();
        //        }
        //    } //MID

        //    using (SqlConnection conn = new SqlConnection(strConn))
        //    {
        //        //string oString = "Select ParentID from Parent where EmailAddr=@user";
        //        SqlCommand oCmd = new SqlCommand("Select ParentID from Parent where EmailAddr = @user", conn);
        //        oCmd.Parameters.AddWithValue("@user", username);
        //        conn.Open();
        //        using (SqlDataReader oReader = oCmd.ExecuteReader())
        //        {
        //            while (oReader.Read())
        //            {
        //                PID = (int)oReader["ParentID"];
        //            }
        //            conn.Close();
        //        }
        //    }
        //}
    }
}