﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_P08_5
{
    public partial class CreatePortfolio : System.Web.UI.Page
    {
        int projectid;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                if ((!string.IsNullOrEmpty(Session["LoginID"] as string)) && (Session["UserType"].Equals("Student")))
                {

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
     
        protected void gv_skillset_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
               Get the SelectedDataKey from GridView, which is the BranchNo,
               and do the necessary data conversion.

               int selectedSkillSet = Convert.ToInt32(gv_skillset.SelectedDataKey[0]);

               Create a skillset object.
               Skillset objSkillset = new Skillset();

               Create a DataSet object to contain the staff list of a branch.
               DataSet result = new DataSet();

               objSkillset.SkillSetID = selectedSkillSet;
            
               int errorCode = objSkillset.getSkillSet(ref result);

               if (errorCode == 0)
               {
                   gv_skillset.DataSource = result;
                   gv_skillset.DataBind();
               }
               */
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Project objStudent = new Project();
            string name = txtName.Text;
            objStudent.title = txtName.Text;
            //objStudent.mentorid = Convert.ToInt32(ddlMentor.SelectedValue);
            string Email = txtEmailAddr.Text;
            objStudent.Description = txtEmailAddr.Text;
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                    ("insert into [Project] (Title,Description) values(@name,@emailaddr)", conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text);
            cmd.Parameters.AddWithValue("@emailaddr", txtEmailAddr.Text);
          //  cmd.Parameters.AddWithValue("@mentorid", ddlMentor.SelectedIndex);
          //  cmd.Parameters.AddWithValue("@course", ddlMentor.SelectedIndex);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Open();
            
            SqlCommand cmd2 = new SqlCommand
               ("SELECT ProjectID FROM Project WHERE Title =@uname AND Description =@pw", conn);

            cmd2.Parameters.AddWithValue("@uname", txtName.Text);
            cmd2.Parameters.AddWithValue("@pw", txtEmailAddr.Text);
            cmd2.CommandType = CommandType.Text;
            SqlDataReader dr = cmd2.ExecuteReader();
            //  using (SqlDataReader dr = cmd2.ExecuteReader())
            if(dr.Read())
            {
                projectid = (int)dr["ProjectID"];
            }
            conn.Close();


            student stud = new student();
         
            int sid = stud.getstudentId(Session["LoginID"].ToString());
            SqlCommand cmd1 = new SqlCommand
                    ("insert into [ProjectMember] (ProjectID,StudentID,Role,Reflection) values(@name,@emailaddr,@role,@reflection)", conn);
            cmd1.Parameters.AddWithValue("@name", projectid);
            cmd1.Parameters.AddWithValue("@emailaddr", sid);
            cmd1.Parameters.AddWithValue("@role","Leader");
            cmd1.Parameters.AddWithValue("@reflection", "I have improved my time-management skill after doing this project.");
            conn.Open();
            cmd1.ExecuteScalar();
            conn.Close();
            lblMessage.Text = "Portoflio Added Successfully";
        }

       

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtEmailAddr.Text = string.Empty;
                txtName.Text = string.Empty;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}

